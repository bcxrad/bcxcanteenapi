﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using BCXCanteenAPI.Adapters;
using BCXCanteenAPI.AdapterViews;
using BCXCanteenAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BCXCanteenAPI.Controllers
{
    [Route("api/[controller]")]
    public class CanteenController : Controller
    {
        
        private CanteenView canteenView;

        public CanteenController() {
            canteenView = new CanteenView();
        }

        [HttpGet("{date}")]
        public CanteenItemList Get(string date)
        {
            return canteenView.GetCanteenItemListFromDate(date);
        }

        [HttpPost]
        public IActionResult Post([FromBody]JObject canteenItem)
        {
            try
            {
                canteenView.InsertCanteenItems(canteenItem);
            } 
            catch (Exception e) 
            {
                return StatusCode(500, e.ToString());
            }
            return StatusCode(200);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
