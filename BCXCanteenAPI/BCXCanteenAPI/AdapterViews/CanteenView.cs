﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using BCXCanteenAPI.Adapters;
using BCXCanteenAPI.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace BCXCanteenAPI.AdapterViews
{
    public class CanteenView
    {
        private const string SELECT_CANTEEN_ITEM_BY_DATE = "SELECT * FROM CANTEEN_MENU WHERE ID in (SELECT ID FROM CANTEEN_MENU_SCHEDULE WHERE AVAILABLE_DATE=@date)";
        private const string INSERT_NEW_CANTEEN_ITEM = "INSERT INTO CANTEEN_MENU (ID, MENU_ITEM_NAME, MENU_ITEM_PRICE, MENU_ITEM_IMAGE_URL) VALUES (@id, @name, @price, @imageUrl)";
        private const string INSERT_NEW_CANTEEN_SCHEDULE = "INSERT INTO CANTEEN_MENU_SCHEDULE (AVAILABLE_DATE) VALUES (@date); SELECT SCOPE_IDENTITY() as id;";
        private const String BLOB_STORAGE_URL = "https://highvelocitystorage.blob.core.windows.net/employee-app/";

        public void InsertCanteenItems(JObject canteenItem) {
            Database db = new Database();
            string idField = "0";

            try
            {
                db.Open();
                SqlCommand sqlComm = (SqlCommand)db.RunQuery(INSERT_NEW_CANTEEN_SCHEDULE);
                sqlComm.Parameters.AddWithValue("@date", (string)canteenItem.GetValue("date"));
                SqlDataReader myReader = sqlComm.ExecuteReader();
                if(myReader.Read()) {
                    idField = myReader["id"].ToString();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                db.Close();
            }

            try {
                db.Open();
                SqlCommand sqlComm = (SqlCommand)db.RunQuery(INSERT_NEW_CANTEEN_ITEM);
                sqlComm.Parameters.AddWithValue("@id", idField);
                sqlComm.Parameters.AddWithValue("@name", (string)canteenItem.GetValue("name"));
                sqlComm.Parameters.AddWithValue("@price", (string)canteenItem.GetValue("price"));
                sqlComm.Parameters.AddWithValue("@imageUrl", (string)canteenItem.GetValue("imageUrl"));
                sqlComm.ExecuteNonQuery();

            } catch (Exception e) {
                throw e;
            } finally {
                db.Close();
            }
        }

        public CanteenItemList GetCanteenItemListFromDate(string date) {
            Database db = new Database();
            CanteenItemList canteenItemList = new CanteenItemList();
            List<CanteenItem> canteenItemsList = new List<CanteenItem>();
            dynamic canteenItems = new JObject();
            try
            {
                db.Open();
                SqlCommand cmd = new SqlCommand(SELECT_CANTEEN_ITEM_BY_DATE, db.GetConnection());
                SqlParameter dateParam = cmd.Parameters.Add("@date", SqlDbType.VarChar);
                dateParam.Value = date;
                SqlDataReader myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    CanteenItem canteenItem = new CanteenItem();
                    canteenItem.id = myReader["id"].ToString();
                    canteenItem.name = myReader["menu_item_name"].ToString();
                    canteenItem.price = myReader["menu_item_price"].ToString();
                    canteenItem.imageUrl = BLOB_STORAGE_URL + myReader["menu_item_image_url"].ToString();
                    canteenItemsList.Add(canteenItem);
                }
                canteenItemList.canteenItemList = canteenItemsList;
                canteenItemList.responseCode = 200;
            }
            catch (Exception e)
            {
                canteenItemList.responseCode = 500;
                canteenItemList.response = e.ToString();
            }
            finally
            {
                db.Close();
            }
            return canteenItemList;
        }
    }
}
