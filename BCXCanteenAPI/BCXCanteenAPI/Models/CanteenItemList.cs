﻿using System;
using System.Collections.Generic;

namespace BCXCanteenAPI.Models
{
    public class CanteenItemList
    {
        public int responseCode { get; set; }
        public string response { get; set; }
        public string transactionId { get; set; }
        public List<CanteenItem> canteenItemList { get; set; }
    }
}
